#include <stdio.h>
#include<math.h>

float input()
{
    float z;
    scanf("%f",&z);
    return z;
}

float eval_dist(float a,float b)
{
    float reslt;
  
	reslt = sqrt(a*a + b*b);
	return reslt;
}   

void output(float p, float q, float out)
{
    printf("The distance between the points %f and %f is %2f",p,q,out);
}

int main()
{
    float x1, x2, y1, y2,x,y,distance;
    printf("For The First point:\n");
    printf("Enter The Value for X:\n");
    x1 = input();
    printf("Enter the Value For Y:\n");
    y1 = input();
    
    printf("For The Second point:\n");
    printf("Enter the Value For X:\n");
    x2 = input();
    printf("Enter the Value For Y:\n");
    y2 = input();
    
    x = (x2 - x1);
    y = (y2 - y1);
    
    distance = eval_dist(x, y);
    output(x,y,distance);

    return 0;
} 
