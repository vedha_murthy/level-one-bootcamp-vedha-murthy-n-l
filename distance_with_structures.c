#include <stdio.h>
#include <math.h>

struct plane_point
{
    float x1, x2, y1, y2;
} R,T;

float inp_pnt()
{
    printf("The First point:\n");
    printf("Enter The Value for X:\n");
    scanf("%f",&R.x1);
    printf("Enter the Value For Y:\n");
    scanf("%f",&R.y1);
    printf("The First point:\n");
    printf("Enter The Value for X:\n");
    scanf("%f",&T.x2);
    printf("Enter the Value For Y:\n");
    scanf("%f",&T.y2);
}

float show_out(float first_Point,float secnd_Point,float Distance)
{
   printf("The distance between the two points %f and %f is: %f\n", first_Point, secnd_Point,Distance);
  
}
float eval_dist()
{
    float first_Point,secnd_Point,distance;
    first_Point = T.x2 - R.x1;// here we are finding the distance from the origin for the inputed point.
    secnd_Point = T.y2 - R.y1;// here we are finding the distance from the origin for the inputed point.
    distance = sqrt(first_Point*first_Point + secnd_Point*secnd_Point);// now the distance difference between absolute distance.
    show_out(first_Point,secnd_Point,distance);// the function showing output called in here itself so no need to call it in the main() again.
    return distance;
}

int main()
{
    inp_pnt();
    eval_dist();
    return 0;
}
